import { Component, OnInit } from '@angular/core';
import { BackendService } from '@shared/services/backend.service';
// import { Store } from '@ngrx/store';
// import { AppState } from './store';
declare var parseSomething: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [BackendService],
})
export class AppComponent implements OnInit {
  title = 'scripbox-hackathon';

  ngOnInit(): void {
    parseSomething('{"a":{"b":true,"c":false}}');
  }
  // constructor(private _backend) {

  // }

  // isLoggedIn: string = 'false';
  // constructor(private store: Store<AppState>) {
  //   this.store.subscribe((data) => {
  //     this.isLoggedIn = `${data.auth.isLoggedIn}`;
  //   });
  // }
}
