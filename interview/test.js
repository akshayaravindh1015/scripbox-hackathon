// Page Loading - how the process begins
// HTML tags css how are applied
// which css classes takes precedence.. how css files are loaded and applied to html tags
// image tag load optimisation
// how to define custom functions in prototype objects
// scope .. var let const experiment
// how to do preprocessors work? & what are the advantages
class SomeClass {
  value = 0;
  constructor(val) {
    this.value = val;
  }
  add(input) {
    this.value += input;
    return this;
  }
  subtract(input) {
    this.value -= input;
    return this;
  }
  multiply(input) {
    this.value *= input;
    return this;
  }
}

cl = new SomeClass(5);
// cl.add(5).subtract(2).multiply(4);

// console.log(cl.value);

// const arr = [1, 2, 3, 0, 5, 6, 0, 9, 0];
// //output =  [1, 2, 3, 0, 0, 5, 6, 0, 0];

// for (i = arr.length - 1; i >= 0; i--) {
//   if (arr[i] == 0) {
//     arr.fill(0, i + 1, i + 2);
//   }
// }

// console.log(arr);

// clone = (input) => {
//   if (typeof input !== "object") {
//     return input;
//   }
//   var result = {};
//   Object.keys(input).forEach((el) => {
//     result[el] = clone(input[el]);
//   });
//   return result;
// };

// const obj1 = {
//   one: {
//     two: true,
//     three: {
//       arr: 5,
//     },
//   },
//   four: true,
// };

// // obj2 = Object.assign({}, obj1); // this will only clone the top level
// obj2 = clone(obj1);
// // obj2.one.two = false;
// obj2.four = false;
// console.log(obj1);
// console.log(obj2);

String.prototype.customReverse = function () {
  const input = this;
  let res = "";
  for (i = 0; i < input.length; i++) {
    res = res + input[input.length - i - 1];
  }
  return res;
};

console.log("something".customReverse());
